package utils;

/**
 * Indicates the displacement direction.
 */
public enum Direction {

    /**
     * Possible directions.
     */
    UP, LEFT, RIGHT, DOWN;
}
