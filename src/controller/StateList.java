package controller;

/**
 * 
 * All possible state of the game.
 *
 */
public enum StateList {

    /**
     * Menu state.
     */
    MENU,

    /**
     * In Game state.
     */
    INGAME,

    /**
     * Pause state.
     */
    PAUSE,

    /**
     * Game Over state.
     */
    GAMEOVER;

}
